﻿using AForge.Controls;
using AForge.Video;
using AForge.Video.DirectShow;
using JointFinder.FrameData;
using JointFinder.ServiceOperation;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JointFinder.FrameCapture
{
    public class FrameCaptureOperation : OperationCore
    {
        private IVideoSource videoDevice;
        private DateTime lastFrameAdded;
        private int framePerSecCount;
        private double frameTimeMs;
        public FrameCaptureOperation()
        {
            this.lastFrameAdded = DateTime.Now;
            this.videoDevice = null;
            this.framePerSecCount = 0;
            this.frameTimeMs = 1000 / Properties.Settings.Default.MaximumFrameRate;
        }

        private bool IsOverMaxFrameRate()
        {
            DateTime dateStart = DateTime.Now;
            TimeSpan timeDiff = dateStart - this.lastFrameAdded;
            //if (timeDiff.TotalSeconds < 1)
            //{
            if (timeDiff.TotalMilliseconds < this.frameTimeMs)
            {
                return true;
            }
            else
            {
                this.lastFrameAdded = dateStart;
                return false;
            }
            //this.framePerSecCount++;
            //if (this.framePerSecCount >= Properties.Settings.Default.MaximumFrameRate)
            //    return true;
            //}
            //else
            //{
            //    this.framePerSecCount = 0;
            //    this.lastFrameAdded = dateStart;
            //}
            //return false;
        }

        private void NewFrameCreated(object sender, NewFrameEventArgs eventArgs)
        {
            if (!IsOverMaxFrameRate())
                Frames.Instance.Add(eventArgs.Frame);
        }

        private void SaveVideoPreferences(VideoCapabilities[] videoCapabilities)
        {
            if (Properties.Settings.Default.DebugActive)
            {
                string videoPreference = "";
                foreach (VideoCapabilities video in videoCapabilities)
                {
                    videoPreference += " Frame size: " + video.FrameSize.Width + "x" + video.FrameSize.Height + " ;MaxFrameRate: " + video.MaximumFrameRate + ";BitCount: " + video.BitCount + Environment.NewLine;
                }
                if (File.Exists(Properties.Settings.Default.VideoPropertiesFile))
                    File.WriteAllText(Properties.Settings.Default.VideoPropertiesFile, videoPreference);
            }
        }

        private IVideoSource SnapShotStream()
        {
            JPEGStream jpegSource = new JPEGStream(Properties.Settings.Default.SnapShotUrl);
            jpegSource.Password = Properties.Settings.Default.WebCamPassword;
            jpegSource.Login = Properties.Settings.Default.WebCamUser;
            jpegSource.NewFrame += new NewFrameEventHandler(NewFrameCreated);
            return jpegSource;
        }

        private IVideoSource MJPEGStream()
        {
            MJPEGStream stream = new MJPEGStream(Properties.Settings.Default.MJPEGUrl);
            stream.Password = Properties.Settings.Default.WebCamPassword;
            stream.Login = Properties.Settings.Default.WebCamUser;
            stream.NewFrame += new NewFrameEventHandler(NewFrameCreated);
            return stream;
        }


        public override void Start()
        {
            if (Properties.Settings.Default.UseJPEGStream)
                this.videoDevice = SnapShotStream();
            if (Properties.Settings.Default.UseMJPEGStream)
                this.videoDevice = MJPEGStream();
            if (this.videoDevice != null)
                this.videoDevice.Start();


            // start the video source
            //FilterInfoCollection videoDevices = new FilterInfoCollection(FilterCategory.VideoInputDevice);
            //if (videoDevices.Count != 0)
            //{
            //    videoDevice = new VideoCaptureDevice(jpegSource);
            //    videoDevice = new VideoCaptureDevice(videoDevices[0].MonikerString);
            //    if (videoDevice != null)
            //    {
            //        VideoCapabilities[] videoCapabilities = videoDevice.VideoCapabilities;
            //        if ((videoCapabilities != null) && (videoCapabilities.Length != 0))
            //        {
            //            SaveVideoPreferences(videoCapabilities);
            //            if (videoCapabilities.Length > Properties.Settings.Default.VideoPreference)
            //                videoDevice.VideoResolution = videoCapabilities[Properties.Settings.Default.VideoPreference];
            //            else
            //                videoDevice.VideoResolution = videoDevice.VideoResolution = videoCapabilities[0];
            //            videoDevice.NewFrame += new NewFrameEventHandler(NewFrameCreated);
            //        }
            //        videoDevice.Start();
            //    }
            //}
        }

        public override void Stop()
        {
            if (videoDevice != null)
                videoDevice.Stop();
        }

        protected override void RunOperation()
        {
            throw new NotImplementedException();
        }
    }
}

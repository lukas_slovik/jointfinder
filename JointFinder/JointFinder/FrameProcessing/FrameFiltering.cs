﻿using AForge.Imaging;
using AForge.Imaging.Filters;
using JointFinder.FrameData;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JointFinder.FrameProcessing
{
    public class FrameFiltering
    {
        private static FrameFiltering instance;

        public static FrameFiltering Instance
        {
            get
            {
                if (instance == null)
                    instance = new FrameFiltering();
                return instance;
            }
        }
        private EuclideanColorFiltering euclideanFilter;
        private Grayscale grayFilter;
        private RectangleBlob rectangle;
        private Crop filter;
        private FrameFiltering()
        {
            this.euclideanFilter = new EuclideanColorFiltering();
            this.euclideanFilter.CenterColor = new RGB(Properties.Settings.Default.ColorRed, Properties.Settings.Default.ColorGreen, Properties.Settings.Default.ColorBlue);
            this.euclideanFilter.Radius = Properties.Settings.Default.EuclideanColorRadius;
            this.grayFilter = new Grayscale(Properties.Settings.Default.GrayScaleCr, Properties.Settings.Default.GrayScaleCg, Properties.Settings.Default.GrayScaleCb);
            this.filter = new Crop(new Rectangle(Properties.Settings.Default.CropPositionX,
                Properties.Settings.Default.CropPositionY,
                Properties.Settings.Default.CropWidth,
                Properties.Settings.Default.CropHight));
            this.rectangle = new RectangleBlob();
        }

        public void Filter(CameraFrame newFrame)
        {
            if (Properties.Settings.Default.FrameFilteringActive)
            {
                Bitmap filteredFrame = new Bitmap(newFrame.Frame);
                euclideanFilter.ApplyInPlace(filteredFrame);
                filteredFrame = grayFilter.Apply(filteredFrame);
                this.rectangle = new RectangleBlob();
                newFrame.ModifiedFrame = this.rectangle.CreateRectangles(filteredFrame);
                newFrame.FoundJoint = this.rectangle.FoundValues;
            }
        }

        public Bitmap Crop(Bitmap frame)
        {
            return this.filter.Apply(frame);
        }
    }
}

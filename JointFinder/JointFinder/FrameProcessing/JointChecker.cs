﻿using JointFinder.FrameData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JointFinder.FrameProcessing
{
    public class JointChecker
    {
        private static bool IsCoverageInTolerance(int xCoverage)
        {
            if (Properties.Settings.Default.CropWidth > 0)
                return ((double)xCoverage / Properties.Settings.Default.CropWidth  >= Properties.Settings.Default.JointMinimumCoverage);
            return false;
        }

        private static bool IsLineAngeInTolerance(double slope)
        {
            if (!Double.IsNaN(slope))
                return Math.Abs(slope) <= Properties.Settings.Default.JointSlopeTolerance;
            return false;
        }

        public static bool Check(JointParameter jointData)
        {
            if (IsCoverageInTolerance(jointData.XCoverage))
                return IsLineAngeInTolerance(jointData.Slope);
            return false;
        }
    }
}

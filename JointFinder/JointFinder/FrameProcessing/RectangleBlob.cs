﻿using AForge.Imaging;
using JointFinder.FrameData;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JointFinder.FrameProcessing
{
    public class RectangleBlob
    {
        private BlobCounter blobCounter;
        public JointParameter FoundValues { get; private set; }
        private double[] xValues;
        private double[] yValues;
        public RectangleBlob()
        {
            this.blobCounter = new BlobCounter();
            this.blobCounter.MinWidth = Properties.Settings.Default.RectangleMinWidth;
            this.blobCounter.MinHeight = Properties.Settings.Default.RectangleMinHeight;
            this.blobCounter.FilterBlobs = true;
            FoundValues = new JointParameter();
            this.xValues = null;
            this.yValues = null;
        }

        private void GetPointValues(Rectangle[] rects)
        {
            xValues = new double[rects.Length];
            yValues = new double[rects.Length];
            int index = 0;
            if (Properties.Settings.Default.DebugActive)
                File.AppendAllText(Properties.Settings.Default.SaveFramesTo + "rectangle.txt", Environment.NewLine);
            foreach (Rectangle objectRect in rects)
            {
                xValues[index] = objectRect.Left + (objectRect.Right - objectRect.Left) / 2;
                yValues[index] = objectRect.Top + (objectRect.Bottom - objectRect.Top) / 2;
                if (Properties.Settings.Default.DebugActive)
                    File.AppendAllText(Properties.Settings.Default.SaveFramesTo + "rectangle.txt", objectRect.Left + ";" + objectRect.Right + Environment.NewLine);
                index++;
            }

        }

        private void GetLine(Rectangle[] rects, Bitmap editableImg)
        {
            FoundValues.CalculateXCoverage(rects);
            GetPointValues(rects);
            int inclusiveStart = 0;
            int exclusiveEnd = rects.Length;
            double rsquared;
            double yintercept;
            double slope;
            SimpleLinearRegression.LinearRegression(xValues, yValues, inclusiveStart, exclusiveEnd, out rsquared, out yintercept, out slope);
            FoundValues.Slope = slope;
            FoundValues.YIntercept = yintercept;
            if (Properties.Settings.Default.DebugActive)
                if (!Double.IsNaN(yintercept) && !Double.IsNaN(slope))
                {
                    Graphics g = Graphics.FromImage(editableImg);
                    using (Pen pen = new Pen(Properties.Settings.Default.LineColor, Properties.Settings.Default.LineWidth))
                    {
                        g.DrawLine(pen, 0, (float)yintercept, editableImg.Width, (float)(editableImg.Width * slope + yintercept));
                    }
                    g.Dispose();
                }
        }

        public Bitmap CreateRectangles(Bitmap image)
        {
            this.blobCounter.ProcessImage(image);
            Bitmap blobImage = new Bitmap(image);
            Rectangle[] rects = blobCounter.GetObjectsRectangles();
            if (rects.Length > 0)
            {
                if (Properties.Settings.Default.DebugActive)
                {
                    Graphics g = Graphics.FromImage(blobImage);
                    foreach (Rectangle objectRect in rects)
                    {
                        using (Pen pen = new Pen(Properties.Settings.Default.RectangleColor, Properties.Settings.Default.RectangleLineWidth))
                        {
                            g.DrawRectangle(pen, objectRect);
                        }
                    }
                    g.Dispose();
                }
                GetLine(rects, blobImage);
            }
            return blobImage;
        }
    }
}

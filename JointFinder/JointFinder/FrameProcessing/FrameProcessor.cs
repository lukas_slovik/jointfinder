﻿using AForge.Imaging.Filters;
using JointFinder.FrameData;
using JointFinder.ServiceOperation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace JointFinder.FrameProcessing
{
    public class FrameProcessor : OperationCore
    {
        public FrameProcessor()
        {
            ShouldStop = false;
        }

        public override void Start()
        {
            RepeateTimeOut = Properties.Settings.Default.FrameProcessorTimeOut;
            Thread processThread = new Thread(RepeatedOperation);
            processThread.Start();
        }

        public override void Stop()
        {
            ShouldStop = true;
        }

        protected override void RunOperation()
        {
            CameraFrame newFrame=Frames.Instance.GetFrame();
            if (newFrame != null)
            {
                FrameFiltering.Instance.Filter(newFrame);
                // if has joint add info to databse? or via socket inform app?
                Frames.Instance.AddFilteredFrame(newFrame);
            }
        }
    }
}

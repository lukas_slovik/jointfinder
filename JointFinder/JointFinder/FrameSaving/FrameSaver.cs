﻿using JointFinder.FrameData;
using JointFinder.ServiceOperation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace JointFinder.FrameSaving
{
    public class FrameSaver : OperationCore
    {
        private int index;
        public FrameSaver()
        {
            this.index = 0;
            ShouldStop = false;
        }
        public override void Start()
        {
            RepeateTimeOut = Properties.Settings.Default.FrameSaverTimeOut;
            Thread processThread = new Thread(RepeatedOperation);
            processThread.Start();
        }

        public override void Stop()
        {
            ShouldStop = true;
        }

        protected override void RunOperation()
        {
            if (Properties.Settings.Default.SaveFrames)
            {
                CameraFrame newFrame = Frames.Instance.GetFilteredFrame();
                if (newFrame != null)
                {
                    bool saveFrame = Properties.Settings.Default.DebugActive || newFrame.FoundJoint.HasJoint();
                    if (saveFrame)
                    {
                        if (newFrame.Frame != null)
                        {
                            newFrame.Frame.Save(Properties.Settings.Default.SaveFramesTo
                            + Properties.Settings.Default.OriginalFilePrefix
                            + index + "_" + newFrame.FrameAdded.ToString(Properties.Settings.Default.FrameDateExport)
                            + Properties.Settings.Default.FrameFileExtention, System.Drawing.Imaging.ImageFormat.Png);
                        }
                        if (newFrame.ModifiedFrame != null)
                        {
                            if (newFrame.FoundJoint.HasJoint())
                            {
                                newFrame.ModifiedFrame.Save(Properties.Settings.Default.SaveFramesTo
                                + Properties.Settings.Default.HasJointFilePrefix
                                + index + "_" + newFrame.ModifiedFrameAdded.ToString(Properties.Settings.Default.FrameDateExport)
                                + Properties.Settings.Default.FrameFileExtention, System.Drawing.Imaging.ImageFormat.Png);
                            }
                            else
                            {
                                newFrame.ModifiedFrame.Save(Properties.Settings.Default.SaveFramesTo
                                + Properties.Settings.Default.FilteredFilePrefix
                                + index + "_" + newFrame.ModifiedFrameAdded.ToString(Properties.Settings.Default.FrameDateExport)
                                + Properties.Settings.Default.FrameFileExtention, System.Drawing.Imaging.ImageFormat.Png);
                            }
                        }
                    }
                    newFrame.DisposeFrames();
                }
            }
        }
    }
}

﻿using JointFinder.ServiceOperation;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace JointFinder
{
    public partial class JointFinder : ServiceBase
    {
        private ServiceOperations operations;
        public JointFinder()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            this.operations = new ServiceOperations();
            this.operations.StartServiceOperations();
        }

        protected override void OnStop()
        {
            if(this.operations!=null)
                this.operations.StopServiceOperations();
        }
    }
}

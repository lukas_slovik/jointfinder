﻿using JointFinder.FrameProcessing;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JointFinder.FrameData
{
    public class CameraFrame
    {
        public Bitmap Frame { get; private set; }
        public Bitmap ModifiedFrame { get; set; }
        public DateTime FrameAdded { get; private set; }
        public DateTime ModifiedFrameAdded { get; private set; }
        public JointParameter FoundJoint { get; set; }
        public CameraFrame(Bitmap frame, DateTime frameAdded)
        {
            Frame = FrameFiltering.Instance.Crop(frame);
            FrameAdded = frameAdded;
            ModifiedFrame = null;
            ModifiedFrameAdded = DateTime.Now;
            FoundJoint = new JointParameter();
        }
        public void DisposeFrames()
        {
            if (Frame != null)
                Frame.Dispose();
            if (ModifiedFrame != null)
                ModifiedFrame.Dispose();
        }
    }
}

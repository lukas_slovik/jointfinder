﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JointFinder.FrameData
{
    public class Frames
    {
        private static Frames instance;

        public static Frames Instance
        {
            get
            {
                if (instance == null)
                    instance = new Frames();
                return instance;
            }
        }

        private Queue<CameraFrame> camFrames;
        private object camFramesLock;
        private Queue<CameraFrame> filteredFrames;
        private object filteredFramesLock;
        private Frames()
        {
            this.camFramesLock = new object();
            this.filteredFramesLock = new object();
            this.camFrames = new Queue<CameraFrame>();
            this.filteredFrames = new Queue<CameraFrame>();
        }

        private bool CanAddFrame()
        {
            lock (this.camFramesLock)
            {
                return (this.camFrames.Count < Properties.Settings.Default.MaximumFramesQueued);
            }
        }

        public CameraFrame Add(Bitmap frameData)
        {
            if (CanAddFrame())
            {
                CameraFrame frm = new CameraFrame(frameData, DateTime.Now);
                lock (this.camFramesLock)
                {
                    this.camFrames.Enqueue(frm);
                }
                return frm;
            }
            return null;
        }

        public CameraFrame GetFrame()
        {
            lock (this.camFramesLock)
            {
                if (this.camFrames.Count > 0)
                    return this.camFrames.Dequeue();
            }
            return null;
        }

        public void AddFilteredFrame(CameraFrame frm)
        {
            if (frm != null)
            {
                lock (this.filteredFramesLock)
                {
                    if (this.filteredFrames.Count < Properties.Settings.Default.MaximumFilteredFramesQueued)
                        this.filteredFrames.Enqueue(frm);
                }
            }
        }

        public CameraFrame GetFilteredFrame()
        {
            lock (this.filteredFramesLock)
            {
                if (this.filteredFrames.Count > 0)
                    return this.filteredFrames.Dequeue();
            }
            return null;
        }
    }
}

﻿using JointFinder.FrameProcessing;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JointFinder.FrameData
{
    public class JointParameter
    {
        private bool? jointCalculated;
        public double YIntercept { get; set; }
        public double Slope { get; set; }
        public int XCoverage { get; set; }
        public void CalculateXCoverage(Rectangle[] rects)
        {
            int[] xValues = new int[Properties.Settings.Default.CropWidth];
            for (int x = 0; x < xValues.Length; x++)
                xValues[x] = 0;
            foreach (Rectangle objectRect in rects)
            {
                for (int x = objectRect.Left; x <= objectRect.Right; x++)
                {
                    if (x == 0)
                        xValues[x] = 1;
                    else
                        xValues[x - 1] = 1;
                }
            }
            for (int x = 0; x < xValues.Length; x++)
                XCoverage += xValues[x];
        }
        
        public bool HasJoint()
        {
            if (jointCalculated == null)
                jointCalculated = JointChecker.Check(this);
            return (bool)jointCalculated;
        }

        public bool ForceHasJoint()
        {
            this.jointCalculated= JointChecker.Check(this);
            return (bool)jointCalculated;
        }

        public JointParameter()
        {
            jointCalculated = null;
            YIntercept = 0;
            Slope = 0;
            XCoverage = 0;
        }
    }
}

﻿using JointFinder.FrameCapture;
using JointFinder.FrameProcessing;
using JointFinder.FrameSaving;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JointFinder.ServiceOperation
{
    public class ServiceOperations
    {
        private List<OperationCore> operations;
        public void StartServiceOperations()
        {
            this.operations = new List<OperationCore>();
            this.operations.Add(new FrameCaptureOperation());
            this.operations.Add(new FrameProcessor());
            this.operations.Add(new FrameSaver());
            foreach (OperationCore op in this.operations)
            {
                op.Start();
            }
        }

        public void StopServiceOperations()
        {
            foreach (OperationCore op in this.operations)
            {
                op.Stop();
            }
        }
    }
}

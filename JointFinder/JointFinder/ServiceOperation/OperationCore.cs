﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace JointFinder.ServiceOperation
{
    public abstract class OperationCore
    {
        private int GetSleepTimeAmount(DateTime startTime)
        {
            TimeSpan difference = DateTime.Now - startTime;
            if (difference.TotalMilliseconds >= RepeateTimeOut)
                return 0;
            else
                return (int)RepeateTimeOut - (int)difference.TotalMilliseconds;
        }

        protected void WaitForTimeOut(DateTime startTime)
        {
            while (!ShouldStop && GetSleepTimeAmount(startTime) > 0)
            {
                Thread.Sleep(Properties.Settings.Default.NoInterruptionTimeOut);
            }
        }

        public void RepeatedOperation()
        {
            DateTime startTime = DateTime.Now;
            while (!ShouldStop)
            {
                startTime = DateTime.Now;
                RunOperation();
                WaitForTimeOut(startTime);
            }
        }

        public abstract void Start();
        public abstract void Stop();
        protected abstract void RunOperation();
        protected int? RepeateTimeOut { get; set; }
        protected bool ShouldStop { get; set; }
    }
}

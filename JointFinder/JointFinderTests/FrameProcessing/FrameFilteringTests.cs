﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using JointFinder.FrameProcessing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using JointFinder.FrameData;
using System.Drawing;

namespace JointFinder.FrameProcessing.Tests
{
    [TestClass()]
    public class FrameFilteringTests
    {
        [TestMethod()]
        public void FilterTest()
        {
            List<CameraFrame> frames = new List<CameraFrame>();
            int index = 0;
            foreach (string file in Directory.EnumerateFiles(@"C:\Users\shaggy\Desktop\Frames1", "*.png"))
            {
                CameraFrame newFrame = new CameraFrame(FrameFiltering.Instance.Crop(new Bitmap(file)) , DateTime.Now);
                FrameFiltering.Instance.Filter(newFrame);
                Bitmap filteredPic = new Bitmap(newFrame.ModifiedFrame);
                frames.Add(newFrame);
                if(newFrame.FoundJoint.HasJoint())
                filteredPic.Save(@"C:\Users\shaggy\Desktop\Frames1\Modified\" + Path.GetFileName(file) , System.Drawing.Imaging.ImageFormat.Png);
                index++;
            }
            Assert.Fail();
        }

        [TestMethod()]
        public void MostComonColor()
        {
            int[,,] commonColors = new int[256, 256, 256];
            foreach (string file in Directory.EnumerateFiles(@"C:\Users\shaggy\Desktop\Frames1", "*.png"))
            {
                Bitmap existingBit = new Bitmap(file);
                for (int width = 0; width < existingBit.Width; width++)
                {
                    for (int height = 0; height < existingBit.Height; height++)
                    {
                        commonColors[existingBit.GetPixel(width, height).R, existingBit.GetPixel(width, height).G, existingBit.GetPixel(width, height).B]++;
                    }
                }
            }
            for (int x = 0; x < 256; x++)
            {
                for (int y = 0; y < 256; y++)
                {
                    for (int z = 0; z < 256; z++)
                    {
                        if (commonColors[x, y, z] != 0)
                            File.AppendAllText(@"C:\Users\shaggy\Desktop\Frames1\framData.txt", x + ";" + y + ";" + z + ";" + commonColors[x, y, z] + Environment.NewLine);
                    }
                }
            }

            Assert.Fail();
        }
    }
}
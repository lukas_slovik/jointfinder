﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using JointFinder.FrameCapture;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace JointFinder.FrameCapture.Tests
{
    [TestClass()]
    public class FrameCaptureOperationTests
    {
        [TestMethod()]
        public void StartTest()
        {
            FrameCaptureOperation test = new FrameCaptureOperation();
            test.Start();
            int i = 0;
            while (i < 60)
            {
                Thread.Sleep(1000);
                i++;
            }
            Assert.Fail();
        }
    }
}